package main

import (
	// "encoding/json"
	"log"
	"net/http"
	"os"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/websocket"
)

type Conversation struct {
	id        string
	clientIds []string
}

type Server struct {
	clients       map[string]*Client
	conversations map[string]*Conversation
	receive       chan *Message
	register      chan *Client
	unregister    chan *Client
}

func NewServer() *Server {
	return &Server{
		clients:    make(map[string]*Client),
		receive:    make(chan *Message),
		register:   make(chan *Client),
		unregister: make(chan *Client),
	}
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func (s *Server) ServeWs(w http.ResponseWriter, r *http.Request) {
	protocols := websocket.Subprotocols(r)
	tokenString := protocols[0]

	secret, ok := os.LookupEnv("SECRET")
	if !ok {
		secret = "88e295edb96d74984c045591f855c6187be513636d727bdcc6fdee6e7f3d581a9632ad7c33702c9dcc5b2e2655945e558de5fe0fb5866e91a27b0214a804cd95"
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(secret), nil
	})

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok && !token.Valid {
		log.Printf("server jwt Parse claims %v, err %v", claims, err)
	}

	id := claims["id"].(string)
	expires := int(claims["expires"].(float64))
	log.Printf("server jwt Parse claims %v id %v expires %v", claims, id, expires)

	conn, err := upgrader.Upgrade(w, r, http.Header{"Sec-Websocket-Protocol": protocols[1:]})
	if err != nil {
		log.Printf("server Upgrade err %v req %v", err, r)
		return
	}

	client := newClient(id, conn, s)
	s.register <- client

	go client.read()
	go client.write()
}

func (s *Server) run() {
	for {
		select {
		case client := <-s.register:
			s.clients[client.id] = client
		case client := <-s.unregister:
			// log.Printf("server Server run unregister client: %v", client)
			if _, ok := s.clients[client.id]; ok {
				delete(s.clients, client.id)
				close(client.send)
			}
		case message := <-s.receive:
			log.Printf("server run receive message %v", message)
			if msgType := message.MessageType; msgType == TEXT {
				s.routeText(message)
			} else if msgType == READ {
				s.routeRead(message)
			} else if msgType == DELIVERED {
				s.routeDelivered(message)
			}
		}
	}
}

func (s *Server) routeRead(m *Message) {
	log.Printf("server routeRead message %v", m)
	/*
		if client, ok := s.clients[m.ConversationID]; ok {
			b, err := packer.pack(m)
			if err != nil {
				log.Printf("server routeRead pack err %v msg %v", err, m)
			}
			client.send <- b
		} else {
			log.Printf("server routeRead not found for msg %v", m)
		}
	*/
	b, err := packer.pack(m)
	if err != nil {
		log.Printf("server routeRead pack err %v msg %v", err, m)
		return
	}
	for clientID, client := range s.clients {
		if clientID == m.SenderID {
			continue
		}
		client.send <- b
	}
}

func (s *Server) routeDelivered(m *Message) {
	log.Printf("server routeDelivered msg %v", m)
	/*
		if client, ok := s.clients[m.ConversationID]; ok {
			b, err := packer.pack(m)
			if err != nil {
				log.Printf("server routeDelivered pack err %v msg %v", err, m)
			}
			client.send <- b
		} else {
			log.Printf("server routeDelivered not found for msg %v", m)
		}
	*/
	b, err := packer.pack(m)
	if err != nil {
		log.Printf("server routeDelivered pack err %v msg %v", err, m)
		return
	}
	for clientID, client := range s.clients {
		if clientID == m.SenderID {
			continue
		}
		client.send <- b
	}
}

func (s *Server) routeText(m *Message) {
	// log.Printf("server routeText m %v", m)
	/*
		if client, ok := s.clients[m.ConversationID]; ok {
			b, err := packer.pack(m)
			if err != nil {
				log.Printf("server routeText pack err %v msg %v", err, m)
			}
			client.send <- b
		} else {
			log.Printf("server routeText not found for msg %v", m)
		}
	*/
	b, err := packer.pack(m)
	if err != nil {
		log.Printf("server routeText pack err %v msg %v", err, m)
		return
	}
	for clientID, client := range s.clients {
		if clientID == m.SenderID {
			continue
		}
		client.send <- b
	}
}
