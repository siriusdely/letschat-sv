package main

import (
	"log"
	"time"

	"github.com/gorilla/websocket"
)

const (
	writeWait      = 10 * time.Second
	pongWait       = 60 * time.Second
	pingPeriod     = (pongWait * 9) / 10
	maxMessageSize = 512
)

type Client struct {
	id   string
	conn *websocket.Conn
	serv *Server
	send chan []byte
}

func newClient(id string, conn *websocket.Conn, serv *Server) *Client {
	return &Client{id, conn, serv, make(chan []byte, 256)}
}

var packer = GetPacker()

func (c *Client) read() {
	defer func() {
		c.serv.unregister <- c
		c.conn.Close()
	}()

	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(string) error {
		c.conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})

	for {
		_, msg, err := c.conn.ReadMessage()
		if err != nil {
			// log.Printf("client read ReadMessage err %v msg %v", err, msg)
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				log.Printf("client read ReadMessage IsUnexpectedCloseError %v msg %v", err, msg)
			}
			break
		}

		message, err := packer.unpack(msg)
		if err != nil {
			log.Printf("client read unpack err %v msg %v", err, msg)
			return
		}
		// log.Printf("client read unpack message %v msg %v", message, msg)

		if msgType := message.MessageType; msgType == TEXT {
			messageID, err := recorder.save(message)
			if err != nil {
				log.Printf("client read save err %v m %v", err, msg)
			}
			message.Content = message.MessageID
			message.MessageID = messageID

			received := &Message{
				MessageType:    RECEIVED,
				ConversationID: message.ConversationID,
				SenderID:       message.SenderID,
				MessageID:      messageID,
			}
			go c.receiveText(received)
		}

		c.serv.receive <- message
	}
}

func (c *Client) write() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.conn.Close()
	}()

	for {
		select {
		case msg, ok := <-c.send:
			// log.Printf("client write msg %v ok %v", msg, ok)
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// log.Printf("client write msg %v ok %v", msg, ok)
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}
			// c.conn.WriteMessage(websocket.BinaryMessage, msg)

			w, err := c.conn.NextWriter(websocket.BinaryMessage)
			if err != nil {
				return
			}
			w.Write(msg)

			n := len(c.send)
			for i := 0; i < n; i++ {
				w.Write(<-c.send)
			}

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		}
	}
}

var recorder = GetRecorder()

func (c *Client) receiveText(ack *Message) {
	// log.Printf("client receiveText ack %v", ack)

	msg, err := packer.pack(ack)
	if err != nil {
		log.Printf("client receiveText pack err %v ack %v", err, msg)
	}

	c.send <- msg
	// close(c.send)
}
