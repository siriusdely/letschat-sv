package main

type MessageType int32

const (
	TEXT      MessageType = 1
	RECEIVED  MessageType = 2
	DELIVERED MessageType = 3
	READ      MessageType = 4
)

type Message struct {
	MessageType    MessageType
	ConversationID string
	SenderID       string
	MessageID      string
	Content        string
}
