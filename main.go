package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

var server = NewServer()

func main() {
	go server.run()

	http.HandleFunc("/", httpHandler)

	port, ok := os.LookupEnv("PORT")
	if !ok {
		port = "3000"
	}

	log.Printf("LetsChat is running on PORT: %v.\n", port)

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", port), nil))
}

func httpHandler(w http.ResponseWriter, r *http.Request) {
	upgrade := false
	for _, header := range r.Header["Upgrade"] {
		if header == "websocket" {
			upgrade = true
			break
		}
	}

	if upgrade == false {
		if r.Method != "GET" {
			http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}
		if r.URL.Path != "/" {
			http.NotFound(w, r)
			return
		}
		fmt.Fprint(w, "Welcome to LetsChat")
		return
	}

	server.ServeWs(w, r)
}
