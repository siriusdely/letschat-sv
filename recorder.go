package main

import (
	"fmt"
	"sync"
	"time"
)

type Recorder struct{}

var recorderSingleton *Recorder
var recorderOnce sync.Once

func GetRecorder() *Recorder {
	recorderOnce.Do(func() {
		recorderSingleton = &Recorder{}
	})
	return recorderSingleton
}

func (r *Recorder) save(m *Message) (string, error) {
	now := time.Now().UnixNano() / (int64(time.Millisecond) / int64(time.Nanosecond))
	messageID := fmt.Sprintf("%v_%v_%v_%v", m.ConversationID, m.SenderID, m.MessageType, now)
	return messageID, nil
}
