package main

import (
	"sync"

	"github.com/shamaton/msgpack"
)

type Packer struct{}

var packerSingleton *Packer
var packerOnce sync.Once

func GetPacker() *Packer {
	packerOnce.Do(func() {
		msgpack.StructAsArray = true
		packerSingleton = &Packer{}
	})
	return packerSingleton
}

func (p *Packer) unpack(b []byte) (*Message, error) {
	var message Message
	err := msgpack.Decode(b, &message)
	if err != nil {
		return nil, err
	}
	return &message, nil
}

func (p *Packer) pack(msg *Message) ([]byte, error) {
	b, err := msgpack.Encode(msg)
	if err != nil {
		return nil, err
	}
	return b, nil
}
